FROM openjdk:16-slim as builder

RUN mkdir -p /app/src
WORKDIR /app/src
COPY . /app/src

RUN ./gradlew build

FROM openjdk:16-slim

COPY --from=builder /app/src/build/libs/almanac-bot-java-*.jar /app/almanac-bot-java.jar

ENTRYPOINT ["java", "-jar","/app/almanac-bot-java.jar"]
